const codeEvent = require('../google-auth/events');

module.exports = function program(req, res) {
  const code = (req.query.code !== undefined) ? req.query.code : null;
  if (code === null) {
    res.status(400).send('Server error');
  } else {
    codeEvent.emit('newGoogleCode', code);
    res.status(200).send('ok');
  }
};

const getProgram = require('../methods/getProgram');

module.exports = function program(req, res) {
  const id = req.params.id;
  const type = req.params.type;
  const time = (req.query.time !== undefined) ? req.query.time : null;
  getProgram(id, time)
    .then((data) => {
      if (type === 'html') {
        if (id) {
          return res.render('program_view', {
            data
          });
        }
        return res.render('program', {
          data
        });
      }
      return res.status(200).send({
        data
      });
    })
    .catch((err) => {
      console.error(err);
      res.status(500).send('Server error');
    });
};

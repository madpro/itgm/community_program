const fs = require('fs');

module.exports = function removeFile(filePath) {
  if (fs.existsSync(filePath)) {
    fs.unlink(filePath, (err) => {
      if (err) console.error(err);
    });
  }
};

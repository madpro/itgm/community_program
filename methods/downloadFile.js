const crypto = require('crypto');
const url = require('url');
const http = require('http');
const https = require('https');
const fs = require('fs');
const path = require('path');

const google = require('googleapis');
const googleAuth = require('google-auth-library');

const createThumb = require('./createThumb');
const removeFile = require('./removeFile');

const config = require('../config');

const drive = google.drive('v3');
const auth = new googleAuth(); //eslint-disable-line
const oauth2Client = new auth.OAuth2(config.google.clientId, config.google.clientSecret, config.google.redirectUrl);
oauth2Client.credentials = config.google.token;

module.exports = function downloadFile(link) {
  const q = url.parse(link);
  const hash = crypto.createHash('sha256');
  hash.update(link);
  const fileHash = hash.digest('hex');
  const fileName = `${fileHash}.jpg`;
  const fileNameThumb = `${fileHash}_100.jpg`;
  const filePath = path.join(config.storage, fileName);
  const filePathThumb = path.join(config.storage, fileNameThumb);
  return new Promise((resolve, reject) => {
    if (fs.existsSync(filePath)) {
      return resolve(fileName);
    }
    const file = fs.createWriteStream(filePath);

    function dCb(err) {
      if (err) {
        removeFile(filePath);
        return reject(err);
      }
      file.close();
      return createThumb(filePath, filePathThumb)
        .then(() => resolve(fileName))
        .catch(err => reject(err));
    }
    if (q.hostname === 'drive.google.com') {
      const fi = q.path.split('/');
      const params = {
        auth: oauth2Client,
        fileId: fi[fi.length - 2],
        alt: 'media'
      };
      drive.files.get(params, dCb)
        .pipe(file);
      return null;
    } else if (q.hostname === 'www.dropbox.com') {
      console.log(q);
      reject({
        code: 500,
        message: 'dropbox.com'
      });
      return null;
    }

    function resCb(res) {
      if (res.statusCode !== 200) {
        removeFile(filePath);
        return reject({
          code: res.statusCode,
          message: res.statusMessage
        });
      }
      res.pipe(file);
      file.on('finish', () => {
        file.close();
        createThumb(filePath, filePathThumb)
          .then(() => resolve(fileName))
          .catch((err) => {
            removeFile(filePath);
            removeFile(filePathThumb);
            return reject(err);
          });
      });
      return null;
    }

    let s = http;

    if (q.protocol === 'https:') {
      s = https;
    }
    return s.get(q, resCb)
      .on('error', err => reject(err));
  });
};

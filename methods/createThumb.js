const gm = require('gm');

module.exports = function createThumb(filePath, filePathThumb) {
  return new Promise((resolve, reject) => {
    gm(filePath)
      .resize(100, 100, '^')
      .quality(100)
      .autoOrient()
      .gravity('Center')
      .crop(100, 100)
      .write(filePathThumb, (err) => {
        if (err) return reject(err);
        return resolve();
      });
  });
};

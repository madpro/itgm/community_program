const crypto = require('crypto');
const url = require('url');

const mkdirp = require('mkdirp');
const google = require('googleapis');
const googleAuth = require('google-auth-library');

const downloadFile = require('./downloadFile');

const sheets = google.sheets('v4');

const config = require('../config');

const auth = new googleAuth(); //eslint-disable-line
const oauth2Client = new auth.OAuth2(config.google.clientId, config.google.clientSecret, config.google.redirectUrl);
oauth2Client.credentials = config.google.token;

module.exports = function sync() {
  mkdirp.sync(config.storage);

  sheets.spreadsheets.values.get({
    auth: oauth2Client,
    spreadsheetId: '1OLZ4D85iR_84e36HfuFfG-fMUL7Zgs8L5ti27GhddkY',
    range: 'C2:J',
  }, (err, response) => {
    if (err) {
      console.log('The API returned an error: ', err);
      return;
    }
    // idx 0 - Community, 1 - Time, 2 - Speaker, 3 - Company, 4 - Subject, 5 - Description, 7 - Photo
    const program = [];

    let q = 0;

    function saveToCommunity(c, speaker) {
      let community = program.find(p => p.community === c);
      if (!community) {
        const hash = crypto.createHash('sha256');
        hash.update(c);
        community = {
          id: hash.digest('hex'),
          community: c,
          program: [speaker]
        };
        program.push(community);
      } else {
        community.program.push(speaker);
      }
      community.program = community.program.sort((a, b) => {
        if (a.time < b.time) return -1;
        if (a.time > b.time) return 1;
        return 0;
      });
      if (response.values.length === q) {
        global.redis.set('program', JSON.stringify(program));
      }
    }

    if (response && response.values) {
      let prevCommunity = null;
      response.values.map((row) => {
        let c = (row[0] !== undefined) ? row[0].trim() : null;
        if (c === '') {
          c = prevCommunity;
        }
        prevCommunity = c;
        const speaker = {
          time: (row[1] !== undefined) ? row[1].trim() : null,
          speaker: (row[2] !== undefined) ? row[2].trim() : null,
          company: (row[3] !== undefined) ? row[3].trim() : null,
          subject: (row[4] !== undefined) ? row[4].trim() : null,
          description: (row[5] !== undefined) ? row[5].trim() : null,
          photo: (row[7] !== undefined && row[7] !== null && url.parse(row[7]).protocol) ? row[7].trim() : null,
        };
        if (c !== null && speaker.speaker !== null) {
          if (speaker.photo !== null) {
            downloadFile(speaker.photo)
              .then((file) => {
                q++;
                speaker.photo = `/images/${file}`;
                saveToCommunity(c, speaker);
              })
              .catch((err) => {
                q++;
                console.error(err);
                speaker.photo = null;
                saveToCommunity(c, speaker);
              });
          } else {
            q++;
            saveToCommunity(c, speaker);
          }
        } else {
          q++;
        }
        return null;
      });
    }
  });
};

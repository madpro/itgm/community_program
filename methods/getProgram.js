const moment = require('moment');

module.exports = function getProgram(community = null, time = null) {
  return new Promise((resolve, reject) => {
    global.redis.get('program', (err, data) => {
      if (err) return reject(err);
      if (data !== null && data !== undefined) {
        data = JSON.parse(data);
      }
      if (community) {
        data = data.find(c => c.id === community);
      } else if (time) {
        let ct = +moment().format('H');
        if (ct < 12 || ct > 17) {
          ct = 12;
        }
        let newData = [];
        data.forEach((c) => {
          let t = c.program.filter((p) => {
            const pt = p.time.split('-');
            const s = +pt[0].split('.')[0];
            const e = +pt[1].split('.')[0];
            return (s >= ct && e <= ct);
          });
          if (t) {
            delete c.program;
            t = t.map(v => Object.assign(v, { community: c }));
            newData = newData.concat(t);
          }
        });
        data = newData;
      }
        // data = data.sort((a, b) => {
        //   if (a.time < b.time) return -1;
        //   if (a.time > b.time) return 1;
        //   return 0;
        // });
      return resolve(data);
    });
  });
};

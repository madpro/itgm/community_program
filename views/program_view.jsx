const React = require('react');
const PropTypes = require('prop-types');
const DefaultLayout = require('./default');

function ProgramView(props) {
  const { data } = props;
  const noPhoto = 'http://piter-united.ru/img/purple/logo.png';
  return (
    <DefaultLayout>
      <div className="container">
        <ol className="breadcrumb">
          <li><a href="/program.html">Программа</a></li>
          <li className="active"><a href={`/program/${data.id}.html`}>{data.community}</a></li>
        </ol>
        <div className="timeline">
          {data.program.map(v =>
            <div className="entry" key={JSON.stringify(v)}>
              <div className="title">
                <h3>{v.time}</h3>
              </div>
              <div className="body">
                {(v.photo !== null) ? <img src={v.photo.replace('.jpg', '_100.jpg')} alt={v.speaker} className="img-circle" /> : <img src={noPhoto} alt={v.speaker} className="img-circle" />}
                <div>
                  {v.speaker} - {v.subject}<br />
                  <small>{v.description}</small>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </DefaultLayout>
  );
}

ProgramView.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string.isRequired,
    community: PropTypes.string.isRequired,
    program: PropTypes.arrayOf(
      PropTypes.shape({
        time: PropTypes.string,
        company: PropTypes.string,
        speaker: PropTypes.string,
        subject: PropTypes.string,
        description: PropTypes.string,
        photo: PropTypes.string
      })
    )
  }).isRequired
};

module.exports = ProgramView;

const React = require('react');
const PropTypes = require('prop-types');

function DefaultLayout(props) {
  return (
    <html lang="ru-RU">
      <head>
        <title>Программа сообществ IT Global Meetup №10</title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="//piter-united.ru/css/purple/style.css" />
        <link rel="stylesheet" href="//piter-united.ru/css/purple/scss.css" />
        <style
          dangerouslySetInnerHTML={{ //eslint-disable-line
            __html: `
            .breadcrumb > li+li:before {
              font-size: 0.8rem;
            }
            .breadcrumb {
              padding-top: 0;
            }
            .breadcrumb > li.active a {
              color: #777;
            }
            .thumbnail-row {
              display: flex;
              justify-content: flex-start;
              flex-direction: row;
              flex-wrap: wrap;
            }
            .thumbnail-row_elm {
              display: flex;
              align-items: flex-start;
            }
            .thumbnail-btn {
              text-decoration: none;
              color: #000;
              cursor: pointer;
              margin: 0;
              background: transparent;
              padding: 0;
              border: 0;
              width: 100%;
            }
            .thumbnail-btn:focus {
              outline: none;
            }
            .img-circle {
              height: 100px;
              margin-right: 20px;
              display: flex;
            }
            .timeline .body {
              display: flex;
            }
            .timeline .body > div {
              display: flex;
              flex-direction: column;
            }
            .timeline .body small {
              font-size: 0.9rem;
            }
            `
          }}
        />
      </head>
      <body>{props.children}</body>
    </html>
  );
}

DefaultLayout.propTypes = {
  children: PropTypes.any.isRequired // eslint-disable-line
};

module.exports = DefaultLayout;

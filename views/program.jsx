const React = require('react');
const PropTypes = require('prop-types');
const DefaultLayout = require('./default');

function ProgramView(props) {
  const { data } = props;
  return (
    <DefaultLayout>
      <div className="container">
        <ol className="breadcrumb">
          <li className="active"><a href="/program.html">Программа</a></li>
        </ol>
        <div className="row text-center thumbnail-row">
          {data.map(v =>
            <div className="col-sm-3 thumbnail-row_elm" key={v.id}>
              <a href={`/program/${v.id}.html`} className="thumbnail-btn a-thumbnail">
                <div className="thumbnail">
                  <h3>{v.community}</h3>
                </div>
              </a>
            </div>
          )}
        </div>
      </div>
    </DefaultLayout>
  );
}

ProgramView.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      community: PropTypes.string.isRequired,
      program: PropTypes.arrayOf(
        PropTypes.shape({
          time: PropTypes.string,
          company: PropTypes.string,
          speaker: PropTypes.string,
          subject: PropTypes.string,
          description: PropTypes.string,
          photo: PropTypes.string
        })
      )
    })
  ).isRequired
};

module.exports = ProgramView;

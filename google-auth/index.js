const googleAuth = require('google-auth-library');
const codeEvent = require('./events');

const SCOPES = ['https://www.googleapis.com/auth/drive.readonly', 'https://www.googleapis.com/auth/spreadsheets.readonly'];

module.exports = function authorize(credentials) {
  const clientSecret = credentials.clientSecret;
  const clientId = credentials.clientId;
  const redirectUrl = credentials.redirectUrl;
  const auth = new googleAuth(); //eslint-disable-line
  const oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

  // Check if we have previously stored a token.
  const authUrl = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });
  console.log('Authorize this app by visiting this url: ', authUrl);
  codeEvent.on('newGoogleCode', (code) => {
    oauth2Client.getToken(code, (err, token) => {
      if (err) {
        console.log('Error while trying to retrieve access token', err);
        return;
      }
      console.log(token);
    });
  });
};

# IT Global Meetup Bot & Program
[![build status](https://gitlab.com/madpro/community_program/badges/master/build.svg)](https://gitlab.com/madpro/community_program/commits/master)

## Install
- npm install

## Configure system
- copy config.default.js to config.js and fix params

## Start bot
- node bot/index.js

## Star program
- node index.js

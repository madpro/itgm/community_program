module.exports = {
  // Telegram token
  telegram: '',
  // Telegram workers
  workers: 4,
  // Telegram admin pwd
  adminPwd: '',
  // Server port
  port: 3000,
  // Storage path
  storage: '',
  // Google api settings
  google: {
    clientId: '',
    clientSecret: '',
    redirectUrl: '',
    token: {
      access_token: '',
      refresh_token: '',
      token_type: '',
      expiry_date: null
    }
  },
  // Redis settings
  redis: {
    host: '127.0.0.1',
    port: 6379,
    password: null,
    db: 1,
    prefix: 'itgm__'
  }
};

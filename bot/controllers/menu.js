const path = require('path');
const crypto = require('crypto');
const Telegram = require('telegram-node-bot');
const moment = require('moment');

const config = require('../../config');

const getProgram = require('../../methods/getProgram');
const getUser = require('../helpers/getUser');
const updateUser = require('../helpers/updateUser');
const addRating = require('../helpers/addRating');

const TelegramBaseController = Telegram.TelegramBaseController;

class MenuController extends TelegramBaseController {
  menuHandler($) {
    this.inlineMenu($);
  }

  menu($) {
    $.runMenu({
      message: 'Меню',
      options: {
        parse_mode: 'Markdown'
      },
      Меню: () => {
        this.inlineMenu($);
      },
      'Схема размещения сообществ': () => {
        this.schema($);
      }
    });
  }

  inlineMenu($) {
    $.runInlineMenu({
      layout: 1,
      method: 'sendMessage',
      params: ['Чего изволите?'],
      menu: [{
        text: 'Сообщества',
        callback: () => {
          this.communityList($);
        }
      }, {
        text: 'Доклады',
        callback: () => {
          this.speakers($);
        }
      }, {
        text: 'Схема размещения сообществ',
        callback: () => {
          this.schema($);
        }
      }]
    });
  }

  voteForSpeaker($, hash, name, rank) {
    return getUser($)
      .then((u) => {
        if (!u || !u.votes || u.votes.indexOf(hash) === -1) {
          $.sendMessage('Спасибо за Ваш голос.');
          let votes = [];
          if (u && u.votes) {
            votes = u.votes;
          }
          votes.push(hash);
          updateUser(u.id, {
            votes
          }).catch(console.error);
          return addRating(hash, name, rank).catch(console.error);
        }
        return $.sendMessage('Вы уже голосовали за данного докладчика');
      })
      .catch(console.error);
  }

  voteForSpeakerMenu($, hash, name) {
    $.runInlineMenu({
      layout: 5,
      method: 'sendMessage',
      params: ['Поставьте оценку:'],
      menu: [{
        text: '💩',
        callback: () => {
          this.voteForSpeaker($, hash, name, 1);
        }
      }, {
        text: '👎',
        callback: () => {
          this.voteForSpeaker($, hash, name, 2);
        }
      }, {
        text: '👌',
        callback: () => {
          this.voteForSpeaker($, hash, name, 3);
        }
      }, {
        text: '👍',
        callback: () => {
          this.voteForSpeaker($, hash, name, 4);
        }
      }, {
        text: '❤️',
        callback: () => {
          this.voteForSpeaker($, hash, name, 5);
        }
      }]
    });
  }

  community($, c) {
    const menu = [];
    c.program.forEach((p) => {
      menu.push({
        text: `${p.time} - ${p.speaker} - ${p.subject}`,
        callback: () => {
          $.sendMessage(`${p.time}\n${p.speaker} - ${p.subject}\nОписание доклада:\n${p.description}`);
          if (p.photo && p.photo.length > 0) {
            $.sendPhoto({
              path: path.join(config.storage, p.photo.replace('/images', ''))
            });
          }
          // let hash = crypto.createHash('sha256');
          // hash.update(`${c.id}_${p.time}`);
          // hash = hash.digest('hex');
          // getUser($)
          //   .then((u) => {
          //     if (!u || !u.votes || u.votes.indexOf(hash) === -1) {
          //       this.voteForSpeakerMenu($, hash, `${c.community} - ${p.speaker}`);
          //     }
          //   })
          //   .catch(console.error);
        }
      });
    });
    $.runInlineMenu({
      layout: 1,
      method: 'sendMessage',
      params: [`Программа сообещства: ${c.community}`],
      menu
    });
  }

  communityList($) {
    getProgram()
      .then((communityList) => {
        const menu = [];
        communityList.forEach((c) => {
          menu.push({
            text: c.community,
            callback: () => {
              this.community($, c);
            }
          });
        });
        $.runInlineMenu({
          layout: 1,
          method: 'sendMessage',
          params: ['Сообщества:'],
          menu
        });
      })
      .catch(console.error);
  }

  speakers($) {
    getProgram(null, true)
      .then((program) => {
        const menu = [];
        program.forEach((p) => {
          menu.push({
            text: `${p.time} ${p.speaker} (${p.community.community}) - ${p.subject}`,
            callback: () => {
              $.sendMessage(`${p.time}\n${p.speaker} (${p.community.community}) - ${p.subject}\nОписание доклада:\n${p.description}`);
              if (p.photo && p.photo.length > 0) {
                $.sendPhoto({
                  path: path.join(config.storage, p.photo.replace('/images', ''))
                });
              }
              let hash = crypto.createHash('sha256');
              hash.update(`${p.community.id}_${p.time}`);
              hash = hash.digest('hex');
              getUser($)
                .then((u) => {
                  if (!u || !u.votes || u.votes.indexOf(hash) === -1) {
                    this.voteForSpeakerMenu($, hash, `${p.community.community} - ${p.speaker}`);
                  }
                })
                .catch(console.error);
            }
          });
        });
        let msg = 'Стол №1 - 12:00-12:50';
        const t = +moment().format('H');
        if (t > 13 && t < 14) {
          msg = 'Стол №2 - 13:00 - 13:50';
        } else if (t > 14 && t < 16) {
          msg = 'Стол №3 - 15:00 - 15:50';
        } else if (t > 16 && t < 17) {
          msg = 'Стол №3 - 15:00 - 15:50';
        }
        $.runInlineMenu({
          layout: 1,
          method: 'sendMessage',
          params: [msg],
          menu
        });
      })
      .catch(console.error);
  }


  schema($) {
    $.sendMessage('Схема размещения сообществ');
    $.sendPhoto({
      path: path.join(__dirname, '..', '..', 'static', 'schema.jpg')
    });
  }

  get routes() {
    return {
      communityListCommand: 'communityList',
      menuCommand: 'menuHandler',
      schemaCommand: 'schema'
    };
  }
}

module.exports = MenuController;

const config = require('../../config');

const Telegram = require('telegram-node-bot');

const getScopeUser = require('../helpers/getScopeUser');
const getUser = require('../helpers/getUser');
const updateUser = require('../helpers/updateUser');

const MenuController = require('./menu');

const TelegramBaseController = Telegram.TelegramBaseController;

class StartController extends TelegramBaseController {
  getAllUsers() {
    return new Promise((resolve, reject) => {
      global.redis.multi()
        .scard('bigset')
        .smembers('bigset')
        .keys('*', (err, replies) => {
          global.redis.mget(replies, global.redis.print);
        })
        .dbsize()
        .exec((err, replies) => {
          if (err) return reject(err);
          const users = [];
          let c = 0;
          const checkEnd = () => {
            if (c <= 0) {
              resolve(users);
            }
          };
          replies = replies[2].map(r => r.replace('itgm__', '')).filter(v => /^\d+$/.test(v));
          replies.forEach((r) => {
            c++;
            global.redis.get(r, (err, data) => {
              c--;
              if (err || !data) {
                if (err) console.error(err);
                checkEnd();
                return null;
              }
              data = JSON.parse(data);
              users.push(data);
              checkEnd();
              return null;
            });
          });
          return null;
        });
    });
  }

  startHandler($) {
    const text = `Привет! Я бот IT Global MeetUp #10. Я могу поделиться с тобой программой сообществ.
А еще тут ты сможешь голосовать за доклады которые прослушал.
    `;
    $.sendMessage(text);
    const user = getScopeUser($);
    updateUser(user.id, user).catch(console.error);
    const menu = new MenuController();
    menu.menu($);
  }

  ratingSend($) {
    global.redis.multi()
      .scard('bigset')
      .smembers('bigset')
      .keys('*', (err, replies) => {
        global.redis.mget(replies, global.redis.print);
      })
      .dbsize()
      .exec((err, replies) => {
        if (err) return $.sendMessage('Ошибка!');
        const rr = [];
        $.sendMessage('Рейтинги!');
        replies[2].forEach((v) => {
          if (v.indexOf('rating') !== -1) {
            rr.push(v.replace('itgm__', ''));
          }
        });
        let rate = [];
        const sendRating = () => {
          rate = rate.sort((a, b) => a.rate - b.rate).reverse();
          let msg = '';
          rate.forEach((v) => {
            msg += `${v.name} - ${v.rate} (${v.count})\n`;
          });
          $.sendMessage(msg);
        };
        let c = 0;
        rr.forEach((r) => {
          c++;
          global.redis.get(r, (err, data) => {
            c--;
            if (err || !data) {
              if (err) console.error(err);
              if (c === 0) {
                sendRating();
              }
              return null;
            }
            data = JSON.parse(data);
            rate.push({
              name: data.name,
              rate: (data.rate.reduce((p, c) => p + c, 0) / data.rate.length).toFixed(),
              count: data.rate.length
            });
            if (c === 0) {
              sendRating();
            }
            return null;
          });
        });
        return null;
      });
  }

  checkAuth($) {
    return new Promise((resolve, reject) => {
      getUser($)
        .then((user) => {
          if (user.isAuth) {
            return resolve($);
          }
          $.sendMessage('Пароль?');
          return $.waitForRequest
            .then(($) => {
              if ($.message.text === config.adminPwd) {
                updateUser(user.id, {
                  isAuth: true
                }).catch(console.error);
                resolve($);
              } else {
                reject($);
              }
            });
        })
        .catch(() => reject($));
    });
  }


  ratingHandler($) {
    this.checkAuth($)
      .then($ => this.ratingSend($))
      .catch($ => $.sendMessage('Не угодал!'));
  }

  sendAllAction($, msg) {
    this.getAllUsers()
      .then(users => users.forEach(user => $.api.sendMessage(user.chat, msg)))
      .catch(console.error);
  }

  sendAllHandler($) {
    this.checkAuth($)
      .then(($) => {
        $.sendMessage('А теперь сообщение!');
        $.waitForRequest
          .then(($) => {
            if ($.message.text !== 'Отмена') {
              this.sendAllAction($, $.message.text);
            }
          });
      })
      .catch($ => $.sendMessage('Не угодал!'));
  }

  statusAction($) {
    this.getAllUsers()
      .then((users) => {
        const msg = `Всего пользователей: ${users.length}
Голосовавших: ${users.filter(v => v.votes.length > 0).length}
        `;
        $.sendMessage(msg);
      })
      .catch(console.error);
  }

  statusHandler($) {
    this.checkAuth($)
      .then(($) => {
        this.statusAction($);
      })
      .catch($ => $.sendMessage('Не угодал!'));
  }

  get routes() {
    return {
      startCommand: 'startHandler',
      ratingCommand: 'ratingHandler',
      sendAllCommand: 'sendAllHandler',
      statusCommand: 'statusHandler'
    };
  }
}

module.exports = StartController;

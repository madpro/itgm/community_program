const getScopeUser = require('./getScopeUser');

module.exports = function getUser($) {
  let id = null;
  let scopeUser = null;
  if (typeof $ === 'number') {
    id = $;
    scopeUser = {};
  } else {
    scopeUser = getScopeUser($);
    id = scopeUser.id;
  }
  return new Promise((resolve) => {
    global.redis.get(id, (err, user) => {
      if (err || !user) {
        if (err) console.error(err);
        return resolve(scopeUser);
      }
      try {
        user = JSON.parse(user);
      } catch (e) {
        console.error(e);
        return resolve(scopeUser);
      }
      user = Object.assign(scopeUser, user);
      return resolve(user);
    });
  });
};

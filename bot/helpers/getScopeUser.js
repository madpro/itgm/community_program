module.exports = function getScopeUser($) {
  return {
    id: $.message._from._id,
    chat: $.message._chat._id,
    name: `${$.message._from._firstName} ${$.message._from._lastName}`,
    userName: $.message._from._username,
    votes: []
  };
};

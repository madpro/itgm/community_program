const getUser = require('./getUser');

module.exports = function updateUser(id, data) {
  return getUser(id)
    .then(user => new Promise((resolve, reject) => {
      user = Object.assign(user, data);
      global.redis.set(id, JSON.stringify(user), (err) => {
        if (err) return reject(err);
        return resolve(user);
      });
    }))
    .catch(console.error);
};

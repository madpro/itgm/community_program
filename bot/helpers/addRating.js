module.exports = function addRating(hash, name, rank) {
  return new Promise((resolve, reject) => {
    const hName = `rating__${hash}`;
    global.redis.get(hName, (err, data) => {
      if (err) return reject(err);
      if (!data) {
        data = {
          name,
          rate: []
        };
      } else {
        data = JSON.parse(data);
      }
      data.rate.push(rank);
      return global.redis.set(hName, JSON.stringify(data), (err) => {
        if (err) return reject(err);
        return resolve();
      });
    });
  });
};

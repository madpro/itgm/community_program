const config = require('../config');
const redisClient = require('redis');
const Telegram = require('telegram-node-bot');

global.redis = redisClient.createClient(config.redis);

const TextCommand = Telegram.TextCommand;
const RegexpCommand = Telegram.RegexpCommand;

const tg = new Telegram.Telegram(config.telegram, {
  workers: config.workers
});

const PingController = require('./controllers/ping');
const StartController = require('./controllers/start');
const MenuController = require('./controllers/menu');

tg.router
  .when(
    new TextCommand('ping', 'pingCommand'),
    new PingController()
  )
  .when(
    new TextCommand('start', 'startCommand'),
    new StartController()
  )
  .when(
    new TextCommand('rating', 'ratingCommand'),
    new StartController()
  )
  .when(
    new TextCommand('/status', 'statusCommand'),
    new StartController()
  )
  .when(
    new TextCommand('/send', 'sendAllCommand'),
    new StartController()
  )
  .when(
    new RegexpCommand(/(menu|меню)/ig, 'menuCommand'),
    new MenuController()
  )
  .when(
    new RegexpCommand(/сообщества|community/ig, 'communityListCommand'),
    new MenuController()
  )
  .when(
    new RegexpCommand(/схема/ig, 'schemaCommand'),
    new MenuController()
  )
  .otherwise(new MenuController());

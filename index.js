const config = require('./config');
const path = require('path');
const redisClient = require('redis');
const express = require('express');
const bodyParser = require('body-parser');
const expressReactView = require('express-react-views');
const crontab = require('node-crontab');

/** Methods */
const getProgram = require('./methods/getProgram');
const sync = require('./methods/sync');
/** Actions */
const program = require('./actions/program');
const code = require('./actions/code');


const authorize = require('./google-auth');


global.redis = redisClient.createClient(config.redis);

const app = express();
app.use('/images', express.static(config.storage));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'jsx');
app.engine('jsx', expressReactView.createEngine({ beautify: true }));

app.all('/', (req, res) => {
  res.redirect('/program.html');
});

app.get('/program.:type', program);
app.get('/program/:id.:type', program);
app.get('/code', code);

const server = app.listen(process.env.PORT || config.port, () => {
  const port = server.address().port;
  console.log(`Server started http://localhost:${port}`);
});

authorize({
  clientId: config.google.clientId,
  clientSecret: config.google.clientSecret,
  redirectUrl: config.google.redirectUrl
});


getProgram()
  .then((data) => {
    if (!data || data.length === 0) {
      sync();
    }
  })
  .catch((err) => {
    console.error(err);
    sync();
  });

/** Sync program every 20 minutes */
crontab.scheduleJob('*/20 * * * *', () => {
  sync();
});
